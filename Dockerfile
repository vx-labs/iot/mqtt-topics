FROM quay.io/vxlabs/dep as builder

RUN mkdir -p $GOPATH/src/github.com/vx-labs
WORKDIR $GOPATH/src/github.com/vx-labs/iot-mqtt-topics
RUN mkdir release
COPY Gopkg* ./
RUN dep ensure -vendor-only
COPY . .
RUN go test ./... && \
    go build -ldflags="-s -w" -buildmode=exe -a -o /bin/topics ./cmd/topics

FROM alpine
EXPOSE 7995
ENTRYPOINT ["/usr/bin/server"]
RUN apk -U add ca-certificates && \
    rm -rf /var/cache/apk/*
COPY --from=builder /bin/topics /usr/bin/server

