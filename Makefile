all: build
test:
	go test $$(glide nv)
pb::
	go generate ./...
build:
	docker build -t vxlabs/iot-mqtt-topics .
	docker build -t vxlabs/iot-mqtt-topics-e2e -f Dockerfile.e2e .

deploy:
	docker run --rm \
	    -e DOCKER_REGISTRY=$$DOCKER_REGISTRY \
        -e KUBE_URL=$$KUBE_URL \
        -e KUBE_NAMESPACE=$$IOT_KUBE_NAMESPACE \
        -e KUBE_TOKEN=$$IOT_KUBE_TOKEN \
        -e KUBE_DOMAIN=$$KUBE_DOMAIN \
        -e COMMIT_HASH=$$CI_COMMIT_SHA \
        -e ENVIRONMENT_PUBLIC_NAME=mqtt.$$IOT_ENVIRONMENT_NAME \
        -v $$(pwd)/kubernetes-spec.yml.template:/media/template:ro \
        ${DOCKER_REGISTRY}/vxlabs/k8s-deploy

e2e::
	docker run --name=etcd   --net=host -d quay.io/coreos/etcd:v3.2
	docker run --name=topics --net=host -d   -e ETCD_ENDPOINTS=http://localhost:2379 -e BACKEND=etcd vxlabs/iot-mqtt-topics
	docker run --name=client --net=host --rm -e RUN_E2E_TESTS=true -v $$(pwd):/usr/local/app vxlabs/iot-mqtt-topics-e2e

clean-e2e::
	docker rm -f etcd || true
	docker rm -f topics || true

local:: build
	docker tag vxlabs/iot-mqtt-topics quay.io/vxlabs/iot-mqtt-topics:latest
	@echo "image tagged quay.io/vxlabs/iot-mqtt-topics:latest"
