package api

import (
	"log"

	"github.com/vx-labs/iot-mqtt-topics/types"
	"github.com/vx-labs/mqtt-protocol/packet"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

type Client struct {
	api types.TopicsServiceClient
}

func New(conn *grpc.ClientConn) *Client {
	return &Client{
		api: types.NewTopicsServiceClient(conn),
	}
}

type retainedMessageFilterOp func(*types.RetainedMessageSelector)

func HasIDIn(id []string) retainedMessageFilterOp {
	if len(id) == 0 {
		return nil
	}
	return func(s *types.RetainedMessageSelector) {
		s.IdIn = id
	}
}
func MatchTopic(pattern []byte) retainedMessageFilterOp {
	if len(pattern) == 0 {
		return nil
	}
	return func(s *types.RetainedMessageSelector) {
		s.Topic = pattern
	}
}
func ToPublishPacket(f func(*packet.Publish)) func(*types.RetainedMessage) {
	return func(message *types.RetainedMessage) {
		f(&packet.Publish{
			Header: &packet.Header{
				Retain: true,
				Qos:    message.Qos,
			},
			Payload: message.Payload,
			Topic:   message.Topic,
		})
	}
}
func ToPublishPacketE(f func(*packet.Publish) error) func(*types.RetainedMessage) error {
	return func(message *types.RetainedMessage) error {
		return f(&packet.Publish{
			Header: &packet.Header{
				Retain: true,
				Qos:    message.Qos,
			},
			Payload: message.Payload,
			Topic:   message.Topic,
		})
	}
}

type CreateRetainedMessageOpt func(*types.CreateRetainedMessageRequest)

func FromPublishPacket(publish *packet.Publish) CreateRetainedMessageOpt {
	return func(req *types.CreateRetainedMessageRequest) {
		req.Topic = publish.Topic
		req.Payload = publish.Payload
		req.Qos = publish.Header.Qos
	}
}

func WithQoS(qos int32) CreateRetainedMessageOpt {
	return func(req *types.CreateRetainedMessageRequest) {
		req.Qos = qos
	}
}

func WithTopic(topic []byte) CreateRetainedMessageOpt {
	return func(req *types.CreateRetainedMessageRequest) {
		req.Topic = topic
	}
}
func WithPayload(payload []byte) CreateRetainedMessageOpt {
	return func(req *types.CreateRetainedMessageRequest) {
		req.Payload = payload
	}
}

func (c *Client) Create(ctx context.Context, tenant string, opts ...CreateRetainedMessageOpt) (string, error) {
	log.Printf("WARN: ignoring Create retained message")
	return "", nil
}
func (c *Client) List(ctx context.Context, tenant string, filters ...retainedMessageFilterOp) (types.RetainedMessageList, error) {
	selector := &types.RetainedMessageSelector{
		Tenant: tenant,
	}
	for _, apply := range filters {
		if apply != nil {
			apply(selector)
		}
	}
	set, err := c.api.List(ctx, selector)
	if err != nil {
		return nil, err
	}
	return types.RetainedMessageList(set.Messages), nil
}
