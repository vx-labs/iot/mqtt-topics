package main

import (
	"bytes"
	"errors"
	"io"
	"log"
	"net"
	"net/http"
	_ "net/http/pprof"
	"os"
	"os/signal"
	"strings"
	"syscall"

	"github.com/grpc-ecosystem/grpc-opentracing/go/otgrpc"

	"github.com/google/uuid"

	"github.com/sirupsen/logrus"
	"github.com/vx-labs/iot-mqtt-topics/state/nats"
	"github.com/vx-labs/iot-mqtt-topics/state/raft"
	"github.com/vx-labs/iot-mqtt-topics/tracing"
	"github.com/vx-labs/iot-mqtt-topics/types"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/health/grpc_health_v1"
)

type state interface {
	io.Closer
	Create(ctx context.Context, value *types.RetainedMessage) (string, error)
	ByID(tenant, id string) (*types.RetainedMessage, error)
	ByTenant(tenant string) (types.RetainedMessageList, error)
	ByTopicPattern(tenant string, pattern []byte) (types.RetainedMessageList, error)
	Status() grpc_health_v1.HealthCheckResponse_ServingStatus
}
type Topics struct {
	state state
}

func Topic(t string) string {
	for strings.HasPrefix(t, "/") {
		t = strings.TrimPrefix(t, "/")
	}
	return t
}

func main() {
	sigc := make(chan os.Signal, 1)
	signal.Notify(sigc,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT)

	logger := logrus.New()
	if os.Getenv("API_ENABLE_PROFILING") == "true" {
		go func() {
			logger.Println(http.ListenAndServe(":8080", nil))
		}()
	}
	port := ":7995"
	lis, err := net.Listen("tcp", port)
	if err != nil {
		logrus.Fatalf("failed to listen: %v", err)
	}
	tracer := tracing.Instance()
	s := grpc.NewServer(
		grpc.UnaryInterceptor(
			otgrpc.OpenTracingServerInterceptor(tracer),
		),
		grpc.StreamInterceptor(
			otgrpc.OpenTracingStreamServerInterceptor(tracer),
		),
	)
	r := &Topics{}
	switch os.Getenv("BACKEND") {
	case "nats":
		r.state = nats.NewNATSProvider(os.Getenv("NATS_URL"), "events")
	case "raft":
		r.state = raft.NewRaftProvider("TopicsService", "topics-cluster", "serf", "raft", 3)
	default:
		panic("no valid backend were specified")
	}
	go r.ServeHTTPHealth()
	types.RegisterTopicsServiceServer(s, r)
	quit := make(chan struct{})
	go func() {
		for {
			<-sigc
			logger.Infoln("starting server shutdown")
			s.Stop()
			logger.Infoln("shutting down tracing")
			tracing.Close()
			logger.Infoln("tracing shutdown completed")
			logger.Infoln("shutting down topics server")
			r.state.Close()
			logger.Infoln("topics server shutdown completed")
			logger.Infoln("server shutdown completed")
			close(quit)
			return
		}
	}()
	logrus.Infof("serving topic store on %v", port)
	if err := s.Serve(lis); err != nil {
		logrus.Printf("failed to serve: %v", err)
	}
	<-quit
}

func (t *Topics) ServeHTTPHealth() {
	mux := http.NewServeMux()
	mux.HandleFunc("/health", func(w http.ResponseWriter, _ *http.Request) {
		nodestatus := t.state.Status()
		switch nodestatus {
		case grpc_health_v1.HealthCheckResponse_NOT_SERVING:
			w.WriteHeader(http.StatusTooManyRequests)
		case grpc_health_v1.HealthCheckResponse_SERVING:
			w.WriteHeader(http.StatusOK)
		default:
			w.WriteHeader(http.StatusServiceUnavailable)
		}
	})
	log.Println(http.ListenAndServe("[::]:9000", mux))
}

func (t *Topics) Create(ctx context.Context, req *types.CreateRetainedMessageRequest) (*types.CreateRetainedMessageResponse, error) {
	id, err := t.state.Create(ctx, &types.RetainedMessage{
		Id:      uuid.New().String(),
		Payload: req.Payload,
		Qos:     req.Qos,
		Tenant:  req.Tenant,
		Topic:   req.Topic,
	})
	return &types.CreateRetainedMessageResponse{Id: id}, err
}

func (t *Topics) Get(ctx context.Context, selector *types.RetainedMessageSelector) (*types.RetainedMessage, error) {
	if selector.Id == "" {
		return nil, errors.New("selector must include an ID to fetch for get operations")
	}
	message, err := t.state.ByID(selector.Tenant, selector.Id)
	if err != nil {
		return nil, err
	}

	if len(selector.Topic) == 0 ||
		bytes.Compare(message.Topic, selector.Topic) == 0 {
		return message, nil
	}
	return nil, errors.New("retained message not found")
}

func (t *Topics) List(ctx context.Context, selector *types.RetainedMessageSelector) (*types.ListRetainedMessageResponse, error) {
	if selector.Tenant == "" {
		return nil, errors.New("selector must include tenant")
	}
	var messages types.RetainedMessageList
	var err error
	switch {
	case selector.Id != "":
		message, err := t.state.ByID(selector.Tenant, selector.Id)
		if err != nil {
			return nil, err
		}
		messages = append(messages, message)
	case len(selector.IdIn) > 0:
		for _, id := range selector.IdIn {
			message, err := t.state.ByID(selector.Tenant, id)
			if err != nil {
				return nil, err
			}
			messages = append(messages, message)
		}
	case len(selector.Topic) > 0:
		messages, err = t.state.ByTopicPattern(selector.Tenant, selector.Topic)
		if err != nil {
			return nil, err
		}
	default:
		messages, err = t.state.ByTenant(selector.Tenant)
	}
	messages = messages.Filter(selector.ToFilter()...)
	return &types.ListRetainedMessageResponse{
		Messages: messages,
	}, nil
}

func (t *Topics) Delete(ctx context.Context, selector *types.RetainedMessageSelector) (*types.MutatedRetainedMessage, error) {
	return nil, errors.New("not implemented")
}
func (t *Topics) Check(ctx context.Context, in *grpc_health_v1.HealthCheckRequest) (*grpc_health_v1.HealthCheckResponse, error) {
	nodestatus := t.state.Status()
	return &grpc_health_v1.HealthCheckResponse{Status: nodestatus}, nil
}
