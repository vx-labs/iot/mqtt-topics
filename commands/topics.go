package commands

import (
	"context"
	"fmt"
	"log"
	"text/template"

	"google.golang.org/grpc"

	"github.com/manifoldco/promptui"

	"github.com/spf13/cobra"
	"github.com/vx-labs/iot-mqtt-topics/api"
	"github.com/vx-labs/iot-mqtt-topics/types"
)

type EndpointProvider interface {
	Endpoint(service string) (endpoint string, token string)
}

func Topics(ctx context.Context, helper EndpointProvider) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "topics",
		Short: "manage retained messages",
	}
	cmd.AddCommand(list(ctx, helper))
	return cmd
}
func getClient(helper EndpointProvider) *api.Client {
	endpoint, _ := helper.Endpoint("topics")
	conn, err := grpc.Dial(endpoint, grpc.WithInsecure())
	if err != nil {
		log.Fatal(err)
	}
	return api.New(conn)
}

func ToString(b []byte) string {
	return string(b)
}
func topicTemplate() *template.Template {
	tpl := `• {{ .Id | green | bold }}
  {{ "Tenant:"      | faint }} {{ .Tenant }}
  {{ "Topic:"       | faint }} {{ .Topic | toString }}
  {{ "Value:"       | faint }} {{ .Payload | toString }}`
	parsed, err := template.New("").Funcs(promptui.FuncMap).Funcs(template.FuncMap{"toString": ToString}).Parse(fmt.Sprintf("%s\n", tpl))
	if err != nil {
		panic(err)
	}
	return parsed
}

func list(ctx context.Context, helper EndpointProvider) *cobra.Command {
	tpl := topicTemplate()
	c := &cobra.Command{
		Use:     "list",
		Args:    cobra.MinimumNArgs(1),
		Aliases: []string{"ls"},
		Run: func(cmd *cobra.Command, topics []string) {
			client := getClient(helper)
			tenant, err := cmd.Flags().GetString("tenant")
			if err != nil {
				log.Fatalf("Error: %v", err)
			}
			if tenant == "" {
				log.Fatalln("tenant argument is required")
			}
			ids, err := cmd.Flags().GetStringArray("id")
			if err != nil {
				log.Fatalf("Error: %v", err)
			}
			for _, topic := range topics {
				set, err := client.List(ctx,
					tenant,
					api.HasIDIn(ids),
					api.MatchTopic([]byte(topic)),
				)
				if err != nil {
					log.Printf("WARN: failed to fetch messages for topic %s: %v", topic, err)
					continue
				}
				err = set.ApplyE(func(sess *types.RetainedMessage) error {
					return tpl.Execute(cmd.OutOrStdout(), sess)
				})
				if err != nil {
					log.Printf("WARN: failed to display topic list: %v", err)
				}
			}
		},
	}
	c.Flags().StringP("tenant", "n", "_default", "select given tenant")
	c.Flags().StringArrayP("id", "i", nil, "select given ids")
	return c
}
