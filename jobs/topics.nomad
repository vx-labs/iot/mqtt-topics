job "topics" {
  datacenters = ["dc1"]
  type        = "service"

  constraint {
    operator = "distinct_hosts"
    value    = "true"
  }

  update {
    max_parallel     = 1
    min_healthy_time = "10s"
    healthy_deadline = "1m"
    health_check     = "checks"
    auto_revert      = true
    canary           = 0
  }

  group "topics-store" {
    count = 3

    restart {
      attempts = 5
      interval = "5m"
      delay    = "15s"
      mode     = "delay"
    }

    ephemeral_disk {
      size = 300
    }

    task "topics-store" {
      driver = "docker"

      env {
        CONSUL_HTTP_ADDR = "172.17.0.1:8500"
        MESSAGES_HOST    = "172.17.0.1:4141"
        BACKEND          = "nats"
        NATS_URL         = "nats://servers.nats.discovery.par1.vx-labs.net:4222"
      }

      config {
        force_pull = true
        image      = "quay.io/vxlabs/iot-mqtt-topics:v6.0.0"

        port_map {
          TopicsService = 7995
          health        = 9000
        }
      }

      resources {
        cpu    = 200
        memory = 128

        network {
          mbits = 10
          port  "TopicsService"{}
          port  "health"{}
        }
      }

      service {
        name = "TopicsService"
        port = "TopicsService"
        tags = ["leader"]

        check {
          type     = "http"
          path     = "/health"
          port     = "health"
          interval = "5s"
          timeout  = "2s"
        }
      }
    }
  }
}
