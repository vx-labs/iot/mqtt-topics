package nats

import (
	"context"
	"errors"
	"log"

	"github.com/golang/protobuf/proto"

	"github.com/google/uuid"
	"github.com/nats-io/go-nats-streaming"
	"github.com/vx-labs/iot-mqtt-events"
	"github.com/vx-labs/iot-mqtt-topics/store"
	"github.com/vx-labs/iot-mqtt-topics/types"
	"google.golang.org/grpc/health/grpc_health_v1"
)

const natsTopic = "vx.iot.mqtt"

type topicStore interface {
	Create(message *types.RetainedMessage) error
	ByTopicPattern(tenant string, pattern []byte) (types.RetainedMessageList, error)
	ByTenant(tenant string) (types.RetainedMessageList, error)
	ByID(tenant, id string) (*types.RetainedMessage, error)
}

type natsProvider struct {
	store        topicStore
	nats         stan.Conn
	subscription stan.Subscription
}

func (p *natsProvider) Apply(event *events.MQTTEvent) error {
	switch event.Name {
	case "message_published":
		message := event.GetMessagePublished()
		if message == nil {
			return errors.New("ignoring malformed message_published event")
		}
		if !message.Retain {
			return nil
		}
		return p.store.Create(&types.RetainedMessage{
			Payload: message.Payload,
			Qos:     message.Qos,
			Tenant:  message.Emitter.Tenant,
			Topic:   message.Topic,
		})
	default:
		return nil
	}
}

func NewNATSProvider(host, cluster string) *natsProvider {
	db, err := store.NewMemDBStore()
	if err != nil {
		panic(err)
	}
	p := &natsProvider{
		store: db,
	}
	clientID := uuid.New().String()
	sc, err := stan.Connect(cluster, clientID,
		stan.NatsURL(host),
		stan.Pings(5, 3))
	if err != nil {
		panic(err)
	}
	sub, err := sc.Subscribe(natsTopic, func(m *stan.Msg) {
		ev := &events.MQTTEvent{}
		err := proto.Unmarshal(m.Data, ev)
		if err != nil {
			log.Printf("ERR: failed to decode received event: %v", err)
		}
		err = p.Apply(ev)
		if err != nil {
			log.Printf("WARN: failed to apply %s event: %v", ev.Name, err)
		}
	}, stan.StartAtSequence(0))
	if err != nil {
		panic(err)
	}
	p.nats = sc
	p.subscription = sub
	return p
}

func (p *natsProvider) Close() error {
	p.subscription.Unsubscribe()
	return p.nats.Close()
}
func (e *natsProvider) ByTopicPattern(tenant string, pattern []byte) (types.RetainedMessageList, error) {
	return e.store.ByTopicPattern(tenant, pattern)
}
func (e *natsProvider) ByID(tenant string, id string) (*types.RetainedMessage, error) {
	return e.store.ByID(tenant, id)
}
func (e *natsProvider) ByTenant(tenant string) (types.RetainedMessageList, error) {
	return e.store.ByTenant(tenant)
}
func (e *natsProvider) Status() grpc_health_v1.HealthCheckResponse_ServingStatus {
	return grpc_health_v1.HealthCheckResponse_SERVING
}
func (e *natsProvider) Create(ctx context.Context, value *types.RetainedMessage) (string, error) {
	return "", errors.New("not implemented")
}
