package raft

import (
	"log"

	"github.com/gogo/protobuf/proto"
	"github.com/vx-labs/iot-mqtt-topics/types"
)

const (
	retainedMessageCreated = iota
	retainedMessageUpdated
	retainedMessageDeleted
)

func (e *raftProvider) Apply(payload []byte) error {
	var action func() error
	switch payload[0] {
	case retainedMessageDeleted:
		log.Println("retained message deletion is not implemented yet")
		return nil
	case retainedMessageUpdated:
		fallthrough
	case retainedMessageCreated:
		ev := &types.RetainedMessage{}
		err := proto.Unmarshal(payload[1:], ev)
		if err != nil {
			log.Printf("ERROR: failed to apply raft log: %v", err)
			return err
		}
		action = func() error {
			return e.store.Create(ev)
		}
	}
	return action()
}
