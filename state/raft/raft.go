package raft

import (
	"context"

	"github.com/golang/protobuf/proto"
	"github.com/google/uuid"
	"github.com/vx-labs/iot-mqtt-topics/store"
	"github.com/vx-labs/iot-mqtt-topics/types"
	state "github.com/vx-labs/state-store"
	"google.golang.org/grpc/health/grpc_health_v1"
)

type topicStore interface {
	SetOffset(offset uint64) error
	Offset() uint64
	Dump() ([]byte, error)
	Load([]byte) error
	Create(message *types.RetainedMessage) error
	ByTopicPattern(tenant string, pattern []byte) (types.RetainedMessageList, error)
	ByTenant(tenant string) (types.RetainedMessageList, error)
	ByID(tenant, id string) (*types.RetainedMessage, error)
}

type raftProvider struct {
	store topicStore
	state state.Store
}

func NewRaftProvider(serviceName, clusterServiceName, serfPort, raftPort string, expectedPeers int) *raftProvider {
	store, err := store.NewMemDBStore()
	if err != nil {
		panic(err)
	}
	p := &raftProvider{
		store: store,
	}
	stateStore, err := state.NewStore(p,
		state.WithExpectedPeers(expectedPeers),
		state.WithSerfPortName(serfPort),
		state.WithRaftPortName(raftPort),
		state.WithServiceName(clusterServiceName))
	if err != nil {
		panic(err)
	}
	p.state = stateStore
	go p.state.RegisterStatefulService(serviceName)
	//go p.leaderLoop()
	return p
}
func (e *raftProvider) Topology() *state.ReadTopologyReply {
	return e.state.Topology()
}
func (e *raftProvider) Close() error {
	return e.state.Close()
}
func (e *raftProvider) Create(ctx context.Context, value *types.RetainedMessage) (string, error) {
	id := uuid.New().String()
	value.Id = id
	message, err := proto.Marshal(value)
	if err != nil {
		return "", err
	}
	payload := make([]byte, len(message)+1)
	payload[0] = retainedMessageCreated
	copy(payload[1:], message)
	return id, e.state.Apply(payload)
}
func (e *raftProvider) ByTopicPattern(tenant string, pattern []byte) (types.RetainedMessageList, error) {
	return e.store.ByTopicPattern(tenant, pattern)
}
func (e *raftProvider) ByID(tenant string, id string) (*types.RetainedMessage, error) {
	return e.store.ByID(tenant, id)
}
func (e *raftProvider) ByTenant(tenant string) (types.RetainedMessageList, error) {
	return e.store.ByTenant(tenant)
}
func (e *raftProvider) Status() grpc_health_v1.HealthCheckResponse_ServingStatus {
	status := e.state.Status()
	if status.Member {
		return grpc_health_v1.HealthCheckResponse_SERVING
	} else {
		return grpc_health_v1.HealthCheckResponse_NOT_SERVING
	}
}
