package raft

import (
	"bytes"
	"io"
	"log"
)

func (e *raftProvider) Restore(r io.ReadCloser) error {
	log.Printf("INFO: snapshot restoration requested")
	buf := bytes.NewBuffer([]byte{})
	_, err := io.Copy(buf, r)
	if err != nil {
		return err
	}
	return e.store.Load(buf.Bytes())
}

type CloseableBuffer struct {
	buf *bytes.Buffer
	io.ReadCloser
}

func (c *CloseableBuffer) Read(b []byte) (int, error) { return c.buf.Read(b) }
func (c *CloseableBuffer) Close() error {
	c.buf = nil
	return nil
}

func (e *raftProvider) Snapshot() (io.ReadCloser, error) {
	log.Printf("INFO: snapshot requested")
	buf, err := e.store.Dump()
	if err != nil {
		return nil, err
	}
	return &CloseableBuffer{buf: bytes.NewBuffer(buf)}, nil
}
