package raft

import (
	"testing"

	"github.com/vx-labs/iot-mqtt-topics/store"
	"github.com/vx-labs/iot-mqtt-topics/types"

	"github.com/stretchr/testify/require"
)

func TestSnapshot(t *testing.T) {
	memdb1, err := store.NewMemDBStore()
	require.NoError(t, err)
	err = memdb1.Create(&types.RetainedMessage{
		Id:     "1",
		Tenant: "_default",
		Topic:  []byte("test/a"),
	})
	require.NoError(t, err)
	s := &raftProvider{
		store: memdb1,
	}
	readCloser, err := s.Snapshot()
	require.NoError(t, err)
	require.NotNil(t, readCloser)

	memdb2, err := store.NewMemDBStore()
	require.NoError(t, err)
	s2 := &raftProvider{
		store: memdb2,
	}
	err = s2.Restore(readCloser)
	require.NoError(t, err)
	msg, err := s2.ByID("_default", "1")
	require.NoError(t, err)
	require.Equal(t, "test/a", string(msg.Topic))
}
