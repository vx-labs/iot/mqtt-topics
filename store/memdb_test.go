package store

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/vx-labs/iot-mqtt-topics/types"
)

func TestMemDB(t *testing.T) {
	db, err := NewMemDBStore()
	assert.Nil(t, err)
	assert.Nil(t, db.Create(&types.RetainedMessage{
		Id:      "a1",
		Payload: []byte("bla"),
		Tenant:  "tenant1",
		Topic:   []byte("devices/a/temperature"),
	}))
	assert.Nil(t, db.Create(&types.RetainedMessage{
		Id:      "a2",
		Payload: []byte("bla"),
		Tenant:  "tenant1",
		Topic:   []byte("devices/b/temperature"),
	}))
	assert.Nil(t, db.Create(&types.RetainedMessage{
		Id:      "b1",
		Payload: []byte("bla"),
		Tenant:  "tenant2",
		Topic:   []byte("devices/c/temperature"),
	}))
	m, err := db.ByTenant("tenant1")
	assert.Nil(t, err)
	if !assert.Equal(t, 2, len(m)) {
		t.Fail()
		return
	}
	assert.Equal(t, "a1", m[0].Id)
	assert.Equal(t, "a2", m[1].Id)
	set, err := db.ByTopicPattern("tenant1", []byte("devices/+/+"))
	assert.Nil(t, err)
	assert.Equal(t, 2, len(set))
	set, err = db.ByTopicPattern("tenant1", []byte("devices/#"))
	assert.Nil(t, err)
	assert.Equal(t, 2, len(set))
}

func TestMemDBDump(t *testing.T) {
	db, err := NewMemDBStore()
	assert.Nil(t, err)
	assert.Nil(t, db.Create(&types.RetainedMessage{
		Id:      "a1",
		Payload: []byte("bla"),
		Tenant:  "tenant1",
		Topic:   []byte("devices/a/temperature"),
	}))
	assert.Nil(t, db.Create(&types.RetainedMessage{
		Id:      "a2",
		Payload: []byte("bla"),
		Tenant:  "tenant1",
		Topic:   []byte("devices/b/temperature"),
	}))
	assert.Nil(t, db.Create(&types.RetainedMessage{
		Id:      "b1",
		Payload: []byte("bla"),
		Tenant:  "tenant2",
		Topic:   []byte("devices/c/temperature"),
	}))
	payload, err := db.Dump()
	assert.Nil(t, err)
	assert.NotNil(t, payload)
	db, err = NewMemDBStore()
	assert.Nil(t, db.Load(payload))
	set, err := db.ByTopicPattern("tenant1", []byte("devices/#"))
	assert.Nil(t, err)
	assert.Equal(t, 2, len(set))
}
